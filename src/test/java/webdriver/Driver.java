package webdriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Driver {

    private static WebDriver driver = null;

    private static Browsers BROWSER = Browsers.CHROME;

    private enum Browsers {CHROME}

    public static WebDriver getDriver() {

        if (driver == null) {

            WebDriverManager.firefoxdriver().setup();
            FirefoxBinary firefoxBinary = new FirefoxBinary();
            FirefoxOptions options = new FirefoxOptions();
            options.setBinary(firefoxBinary);
            options.setHeadless(true);  // <-- headless set here
            driver = new FirefoxDriver(options);
        }
        return driver;
    }

    public static void setDriver(WebDriver dri) {
        driver = dri;
    }
}
